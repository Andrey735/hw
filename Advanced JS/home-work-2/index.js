const books = [
  {
    author: "Скотт Бэккер",
    name: "Тьма, что приходит прежде",
    price: 70,
  },
  {
    author: "Скотт Бэккер",
    name: "Воин-пророк",
  },
  {
    name: "Тысячекратная мысль",
    price: 70,
  },
  {
    author: "Скотт Бэккер",
    name: "Нечестивый Консульт",
    price: 70,
  },
  {
    author: "Дарья Донцова",
    name: "Детектив на диете",
    price: 40,
  },
  {
    author: "Дарья Донцова",
    name: "Дед Снегур и Морозочка",
  },
];

const body = document.body;
const list = document.createElement("ul");
const root = document.createElement("div");

root.setAttribute("id", "root");
body.append(root);

const necessKeys = ["author", "name", "price"];

function keyCheck(arr) {
  arr.forEach((objElem) => {
    try {
      let listItem = document.createElement("li");
      necessKeys.forEach((elem) => {
        if (objElem.hasOwnProperty(elem)) {
          listItem.innerText += " " + objElem[elem];
        } else {
          throw new Error(`In your object is not found "${elem}"`);
        }
      });
      list.append(listItem);
    } catch (error) {
      console.log(error.message);
    }
  });
}

keyCheck(books);

root.append(list);
