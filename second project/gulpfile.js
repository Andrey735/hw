const gulp = require("gulp");
const sass = require("gulp-sass")(require("sass"));
const rename = require("gulp-rename");
const clean = require("del");
const concat = require("gulp-concat");
const uglify = require("gulp-uglify");

function moveCss() {
  return gulp
    .src("./src/scss/index.scss")
    .pipe(sass().on("error", sass.logError))
    .pipe(rename("styles.min.css"))
    .pipe(gulp.dest("./dest"));
}

function filesClean() {
  return clean("dest");
}

function moveJS() {
  return gulp
    .src("src/js/*.js")
    .pipe(uglify())
    .pipe(concat("scripts.min.js"))
    .pipe(gulp.dest("./dest"));
}

gulp.task("build", gulp.series(filesClean, moveCss, moveJS));

gulp.task("dev", function () {
  gulp.watch(["./src/scss/**/*.scss", "./src/js/*.js"], gulp.series("build"));
});
