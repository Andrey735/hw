const menuBtn = document.querySelector(".menu-btn");
const navMenu = document.querySelector(".main-nav__list");
const body = document.querySelector("body");
let menuOpen = false;
menuBtn.addEventListener("click", () => {
  if (!menuOpen) {
    menuBtn.classList.add("open");
    navMenu.classList.add("open-menu");
    menuOpen = true;
  } else {
    menuBtn.classList.remove("open");
    navMenu.classList.remove("open-menu");
    menuOpen = false;
  }
});

body.onresize = (e) => {
  if (e.target.innerWidth >= 320) {
    menuBtn.classList.remove("open");
    navMenu.classList.remove("open-menu");
    menuOpen = false;
  }
};
