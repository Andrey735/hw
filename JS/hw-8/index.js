function createNewElement(tagName, text, classNames = [], attributes = {}) {
  let elem = document.createElement(tagName);
  elem.innerText = text;
  elem.classList = classNames;
  for (const key in attributes) {
    elem.setAttributes = {
      key: this[key],
    };
  }
  return elem;
}

const labelText = createNewElement("label", "Price", ["label-text"]);
document.body.append(labelText);

const inputText = createNewElement("input", "", [], {
  type: "text",
});
document.body.append(inputText);

const notValidText = createNewElement("p", "Please enter correct price");

inputText.addEventListener("focus", () => {
  inputText.classList.add("input-focus");
  notValidText.remove();
  inputText.classList.remove("input-not-valid");
});

inputText.addEventListener("blur", () => {
  inputText.classList.remove("input-focus");
  let value = inputText.value;
  if (!Number.isNaN(+value) && value.trim() !== "" && value >= 0) {
    const currentPrice = createNewElement("span", `Текущая цена: ${value}`, [
      "current-price",
    ]);
    document.body.prepend(currentPrice);
    const bttRemove = createNewElement("button", "x", ["btt-remove"]);
    currentPrice.append(bttRemove);
    inputText.style.color = "green";

    bttRemove.addEventListener("click", () => {
      bttRemove.parentNode.remove();
      inputText.value = "";
    });
  } else if (value.trim() === "") {
    notValidText.remove();
  } else {
    inputText.classList.add("input-not-valid");
    document.body.append(notValidText);
    inputText.style.color = "";
  }
});
