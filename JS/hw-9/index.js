const listTabs = document.querySelector(".tabs");
listTabs.style.position = "fixed";
listTabs.style.top = "10px";
let contentElement = document.querySelectorAll(".content-element");
contentElement.forEach((e) => {
  e.style.display = "none";
});

listTabs.addEventListener("click", (event) => {
  const activeTab = listTabs.querySelector(".active");
  if (activeTab) {
    activeTab.classList.remove("active");
  }
  event.target.classList.add("active");

  contentElement.forEach((e) => {
    if (event.target.dataset.active === e.dataset.active) {
      e.style.display = "block";
    } else {
      e.style.display = "none";
    }
  });
});
