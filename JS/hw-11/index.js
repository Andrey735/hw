const buttons = document.querySelectorAll(".btn");
window.addEventListener("keydown", (event) => {
  buttons.forEach((element) => {
    event.code === element.dataset.key
      ? element.classList.add("btn-active")
      : element.classList.remove("btn-active");
  });
});
