let num1;
let num2;
let operator;

do {
  num1 = +prompt("Enter number 1");
  num2 = +prompt("Enter number 2");
} while (
  (!num1 && num1 !== 0) ||
  (!num2 && num2 !== 0) ||
  Number.isNaN(+num1) ||
  Number.isNaN(+num2)
);

do {
  operator = prompt("Enter the operator");
} while (
  operator !== "+" &&
  operator !== "-" &&
  operator !== "*" &&
  operator !== "/"
);

function operation(firsNum, secondNum, symbol) {
  if (symbol === "+") {
    return firsNum + secondNum;
  } else if (symbol === "-") {
    return firsNum - secondNum;
  } else if (symbol === "*") {
    return firsNum * secondNum;
  } else if (symbol === "/") {
    return firsNum / secondNum;
  }
}
console.log(operation(num1, num2, operator));
