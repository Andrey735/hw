const wrapper = document.querySelectorAll(".input-wrapper");
const inputPassword = document.querySelectorAll("input");
const warningText = document.createElement("span");

wrapper.forEach((e) => {
  e.addEventListener("click", (event) => {
    if (event.target.classList.contains("icon-password")) {
      if (
        event.target.previousElementSibling.getAttribute("type") == "password"
      ) {
        event.target.previousElementSibling.setAttribute("type", "text");
      } else {
        event.target.previousElementSibling.setAttribute("type", "password");
      }

      const eye = e.querySelector("i");
      if (event.target.classList.contains("fa-eye")) {
        eye.classList.remove("fa-eye");
        eye.classList.add("fa-eye-slash");
      } else {
        eye.classList.remove("fa-eye-slash");
        eye.classList.add("fa-eye");
      }
    }
  });
});

const btn = document.querySelector(".btn");

btn.addEventListener("click", (event) => {
  event.preventDefault();
  let firstPass = document.querySelector(".firstInput").value;
  let secondPass = document.querySelector(".secondInput").value;

  if (firstPass === secondPass && firstPass && secondPass) {
    alert("You are welcome");
    document.querySelector(".firstInput").value = "";
    document.querySelector(".secondInput").value = "";
    warningText.remove();
  } else {
    warningText.classList.add("warning-text");
    warningText.innerText = "Нужно ввести одинаковые значения";
    document.body.append(warningText);
  }
});
