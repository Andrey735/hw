const images = document.querySelectorAll(".image-to-show");

images[0].style.display = "block";

let showImages = setInterval(
  (foo = () => {
    for (let i = 0; i < images.length; i++) {
      if (images[i].style.display === "block") {
        images[i].style.display = "none";
        if (i === images.length - 1) {
          images[0].style.display = "block";
        } else {
          images[i + 1].style.display = "block";
          return;
        }
      }
    }
  }),
  3000
);

let btnSection = document.querySelector(".btn-section");
let btnStop = document.querySelector(".btn-stop");
let btnStart = document.querySelector(".btn-start");

btnSection.addEventListener("click", (event) => {
  if (event.target.classList.contains("btn-stop")) {
    clearInterval(showImages);
    event.target.setAttribute("disabled", true);
    btnStart.removeAttribute("disabled");
  } else if (event.target.classList.contains("btn-start")) {
    showImages = setInterval(foo, 3000);
    event.target.setAttribute("disabled", true);
    btnStop.removeAttribute("disabled");
  }
});
