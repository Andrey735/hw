function createNewUser() {
  let name = prompt("Enter your name");
  let lastName = prompt("Enter your last name");
  return {
    _firstName: name,
    _lastName: lastName,
    getLogin: function () {
      return (
        this._firstName.slice(0, 1).toLowerCase() + this._lastName.toLowerCase()
      );
    },
    set firstName(value) {
      this._firstName = value;
    },
    set lastName(value) {
      this._lastName = value;
    },
  };
}

let newUser = createNewUser();
console.log(newUser.getLogin());

Object.defineProperty(newUser, "_firstName", {
  writable: false,
});
newUser._firstName = "writable name";
console.log(newUser._firstName);
