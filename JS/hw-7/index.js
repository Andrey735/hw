const array = ["hello", "world", "Kiev", "Kharkiv", "Odessa", "Lviv"];

let getListItem = function (arr, parent = document.body) {
  const newArr = arr.map((e) => {
    return `<li>${e}</li>`;
  });

  parent.insertAdjacentHTML("beforeend", `<ul>${newArr.join(" ")}</ul>`);
};

getListItem(array);

// let getListItem = function (arr, parent = document.body) {
//   const newArr = arr.map((e) => {
//     const listItem = document.createElement("li");
//     listItem.innerHTML = e;
//     return listItem.outerHTML;
//   });

//   parent.insertAdjacentHTML("beforeend", `<ul>${newArr.join(" ")}</ul>`);
// };

// getListItem(array);

// let getListItem = function (arr, parent = document.body) {
//   const newArr = arr.map((e) => {
//     const li = document.createElement("li");
//     li.innerText = e;
//     return li;
//   });

//   const ul = document.createElement("ul");
//   newArr.forEach((element) => {
//     ul.append(element);
//   });
//   parent.append(ul);
// };

// getListItem(array);
