function createNewUser() {
  let name = prompt("Enter your name");
  let lastName = prompt("Enter your last name");
  let dateofBirth = prompt("Enter your date of Birth in format 'dd.mm.yyyy'");
  return {
    _firstName: name,
    _lastName: lastName,
    _birthday: dateofBirth,
    getLogin: function () {
      return (
        this._firstName.slice(0, 1).toLowerCase() + this._lastName.toLowerCase()
      );
    },
    set firstName(value) {
      this._firstName = value;
    },
    set lastName(value) {
      this._lastName = value;
    },
    set birthday(value) {
      this._birthday = value;
    },
    getAge: function () {
      let nowDate = new Date();
      let arrOfBirtday = this._birthday.split(".");
      let userbirthday = new Date(
        arrOfBirtday[2],
        arrOfBirtday[1] - 1,
        arrOfBirtday[0]
      );
      return nowDate.getFullYear() - userbirthday.getFullYear();
    },
    getPassword: function () {
      return (
        this._firstName.slice(0, 1).toUpperCase() +
        this._lastName.toLowerCase() +
        this._birthday.slice(-4)
      );
    },
  };
}

let newUser = createNewUser();
console.log(newUser);
console.log(newUser.getAge());
console.log(newUser.getPassword());

Object.defineProperty(newUser, "_firstName", {
  writable: false,
});
Object.defineProperty(newUser, "_lastName", {
  writable: false,
});

Object.defineProperty(newUser, "_birthday", {
  writable: false,
});
