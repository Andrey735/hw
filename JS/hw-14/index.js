$(document).ready(function () {
  $(".menu a").click(function () {
    let elem = $(this).attr("href");
    console.log(elem);
    let topOfScroll = $(elem).offset().top;

    $("body,html").animate(
      {
        scrollTop: topOfScroll,
      },
      1000
    );
  });

  let btnUp = $(".btnUP");

  $(window).scroll(function () {
    if ($(this).scrollTop() > 450) {
      btnUp.fadeIn();
    } else btnUp.fadeOut();
  });

  btnUp.click(function () {
    $("body,html").animate(
      {
        scrollTop: 0,
      },
      1000
    );
  });

  let btnToggle = $(".toggle-btn");

  btnToggle.click(function () {
    $(".clients").slideToggle(1500);
  });
});
