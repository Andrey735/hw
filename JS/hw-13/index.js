const btnChangeColor = document.querySelector(".btn-change-color");
const linkCss = document.createElement("link");
linkCss.setAttribute("rel", "stylesheet");
linkCss.setAttribute("href", "./css/reserv-styles.css");
linkCss.classList.add("reserv-styles");
linkCss.classList.add("theme");

btnChangeColor.addEventListener("click", () => {
  const hasLink = document.querySelector(".reserv-styles");
  if (!hasLink) {
    document.head.append(linkCss);
    localStorage.setItem("theme", "purpleTheme");
  } else {
    hasLink.remove();
    localStorage.setItem("theme", "basicTheme");
  }
});

window.addEventListener("load", () => {
  const theme = localStorage.getItem("theme");
  if (theme === "purpleTheme") {
    document.head.append(linkCss);
  }
});
