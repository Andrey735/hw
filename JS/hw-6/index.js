const filterBy = (arr, typeOfValue) => {
  return arr.filter((e) => {
    if (e === null) {
      return String(e) !== typeOfValue;
    } else return typeof e !== typeOfValue;
  });
};

const array = ["dfdf", true, "12", 123, null, false, undefined];

allTypes = ["string", "boolean", "number", "null", "undefined"];

allTypes.forEach((type) => {
  console.log(filterBy(array, type));
});
